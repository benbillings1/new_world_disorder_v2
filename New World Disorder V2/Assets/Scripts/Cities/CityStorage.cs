﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Territories;
using Governors;
using Random=UnityEngine.Random;
using System;

public class CityStorage : MonoBehaviour
{
	static string tsvFilePath = "Assets/TSVs/Cities.tsv";

	public static City[] cities;
	public static bool citiesLoaded = false;

	float setFloat(string datum){
		float number;
		if (float.TryParse(datum, out number)){
			return number;
		}
		return 1337f;
	}

	void Start()
	{
		citiesLoaded = false;
		cities = new City[100];

        //This for loop gets information from the city tsv file and adds city objects to an array for every city in the game
		for (int row = 2; row < cities.Length+1; row++)
		{
            //This if statement checks if the row of the tsv file has city information in it
			if (TSVScript.read(tsvFilePath, row, 1) != null)
			{
				CityMetricTraitPack metrics = new CityMetricTraitPack();
				
				metrics.economy = setFloat(TSVScript.read(tsvFilePath, row, 4));
				metrics.education = setFloat(TSVScript.read(tsvFilePath, row, 5));
				metrics.approval = setFloat(TSVScript.read(tsvFilePath, row, 6));
				metrics.crime = setFloat(TSVScript.read(tsvFilePath, row, 7));
				metrics.economy = setFloat(TSVScript.read(tsvFilePath, row, 8));
				
				City city = new City();
				city.name = TSVScript.read(tsvFilePath, row, 1);
				int cityIndex = Random.Range(0, GovernorStorage.regionStorage.Length-1);
				city.region = GovernorStorage.regionStorage[cityIndex];	//TSVScript.read(tsvFilePath, row, 2); //apparently in the TSVs this is set as country?
				city.population = (int)setFloat(TSVScript.read(tsvFilePath, row, 3));
				city.metrics = metrics;
				city.longitude = (setFloat(TSVScript.read(tsvFilePath, row, 10)));
				city.latitude = setFloat(TSVScript.read(tsvFilePath, row, 9));
				
				Agenda agenda = new Agenda();
				agenda.metricRanking = new string[5];
				int economyRank = (int)setFloat(TSVScript.read(tsvFilePath, row, 11))-1;
				//Debug.Log("EconomyRank: " + economyRank);
				if (economyRank != 1336){
					agenda.metricRanking[economyRank] = "Economy";
					agenda.metricRanking[(int)setFloat(TSVScript.read(tsvFilePath, row, 12))-1] = "Life";
					agenda.metricRanking[(int)setFloat(TSVScript.read(tsvFilePath, row, 13))-1] = "Education";
					agenda.metricRanking[(int)setFloat(TSVScript.read(tsvFilePath, row, 14))-1] = "Approval";
					agenda.metricRanking[(int)setFloat(TSVScript.read(tsvFilePath, row, 15))-1] = "Crime";
				}
				city.agenda = agenda;
				
				cities[row-2] = city;

			}
		}
		citiesLoaded = true;

	}
}
