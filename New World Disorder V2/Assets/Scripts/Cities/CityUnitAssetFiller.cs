﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityUnitAssetFiller : MonoBehaviour
{
    public GameObject tower1;
	public GameObject tower2;
    public GameObject tower3;
	public GameObject tower4;
	public GameObject tower5;
	
	public int population = 0;
	
	GameObject[] towers;
	
	int radius = 0; //how far from the center of town things currently are
	

	void createBuilding(float degree, GameObject tower){
		float x = Mathf.Sin(degree) * (float)radius;
		float y = Mathf.Cos(degree) * (float)radius;
		
		Vector3 pos = new Vector3(0,0,0);
		GameObject building = Instantiate(tower, pos, Quaternion.identity);
		building.transform.SetParent(transform);
		building.transform.localRotation = Quaternion.Euler(Random.Range(0,180),-90,-90);
		building.transform.localPosition = new Vector3(x,y,0);
		building.transform.localScale = new Vector3(8,8,8);
	}

	void createTown(){		
		
		foreach (Transform child in transform){
			GameObject.Destroy(child.gameObject);
		}
		radius = 0;
	
		for (int i=1; i<population; i++){
			
			radius = (int)Mathf.Floor(i/8);

			if (radius > 1){
				radius = radius - 1;
			}

			int perimeter = 8 + ((radius-1)*8);
			int buildingIndex = population%8; //what building we are currently on in the current radius index
			float buildingIndexDeg = ((float)buildingIndex)*(360f/8f);
			createBuilding(buildingIndexDeg, towers[Random.Range(0,towers.Length-1)]);
			
		}
		
	}
	
	// Start is called before the first frame update
    void Start()
    {
        towers = new GameObject[]{tower1, tower2, tower3, tower4, tower5};
		createTown();
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
