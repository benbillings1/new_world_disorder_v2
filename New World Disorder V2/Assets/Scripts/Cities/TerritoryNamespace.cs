﻿using System.Collections;
using System.Collections.Generic;
using Governors;
using UnityEngine;

namespace Territories
{
    public class City
    {
        public string name;
        public string region;
        public int population;
		public Agenda agenda;
        //public float qualityOfLife;
        //public float education;
        //public float approvalRate;
        //public float crimeRate;
        //public float economy;
		public CityMetricTraitPack metrics;
		
		public float longitude;
		public float latitude;

        public City(){}

        public override string ToString()
        {
            return "Name: " + name + ", " + "Region: " + region + ", " + "Population :" + population;
        }


    }


    
        
}
