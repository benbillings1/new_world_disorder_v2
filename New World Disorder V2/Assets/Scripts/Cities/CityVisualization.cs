﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Territories;

public class CityVisualization : MonoBehaviour
{
	bool createdCities;
    public GameObject cityObject;
	public GameObject cityBlock;
	public GameObject earth;
    //public float timePassed;
    // Start is called before the first frame update
    void Start(){
		createdCities = false;
	}

	Vector3 ComputeLocation(float longitudinalRotationDegree, float latitudinalRotationDegree, float radius)
	{

		longitudinalRotationDegree = longitudinalRotationDegree + 90;

		float longitudinalRotation = (Mathf.PI*2/360)*(longitudinalRotationDegree);
		float latitudinalRotation = (Mathf.PI*2/360)*(latitudinalRotationDegree);
		
		Vector3 globePosition = new Vector3(earth.transform.position.x, earth.transform.position.y, earth.transform.position.z); //assign this the position of the globe//apply first angle

		//setting latitudinal point
		float A = Mathf.Sin(latitudinalRotation)*radius; //a side of lat triangle
		float B = Mathf.Cos(latitudinalRotation)*radius; //b side of lat triangle
		float Bx = Mathf.Sin(longitudinalRotation)*B; //x aspect of B position
		float Bz = Mathf.Cos(longitudinalRotation)*B; //z aspect of B position

		Vector3 finalPoint = new Vector3(globePosition.x+Bx, globePosition.y+A, globePosition.x+Bz); //this should be the final global position of the point.

		return finalPoint;
	}

	// Update is called once per frame
	void Update(){
		if (CityStorage.citiesLoaded == true && createdCities == false){
			createdCities = true;
			//create cities if not created
			for (int i = 0; i < CityStorage.cities.Length; i++){
				City city = CityStorage.cities[i];
				if (city != null && city.name != ""){
                    float lon = city.longitude;
					if (lon < 0 && lon > -90){
						lon = lon * -1;
					}else if(lon > 90){
						lon = lon * -1; 
					}else if(lon <= 90 && lon > 0){
						lon = lon * -1;
					}
					float lat = city.latitude;
					float radius = 4.94f + (city.population*0.005f); //assign this
					Vector3 pos = ComputeLocation(lon, lat, radius);
					GameObject cB = Instantiate(cityBlock, pos, Quaternion.identity);
					
					cB.GetComponent<CityUnitAssetFiller>().population = city.population;
					
					cB.transform.position = pos;
					cB.name = city.name;
					cB.transform.parent = transform;
					
					Vector3 targetDirection = earth.transform.position - cB.transform.position;

					Vector3 newDirection = Vector3.RotateTowards(cB.transform.forward, targetDirection, 6.28319f, 360f);

					Debug.DrawRay(cB.transform.position, newDirection, Color.red);

					cB.transform.rotation = Quaternion.LookRotation(newDirection);
				}
			}			
		}

        /*timePassed += Time.deltaTime;
        if (timePassed > 10)
        {
            for(int i = 0; i < CityStorage.cities.Length - 1; i++)
            {
                if (CityStorage.cities[i] != null)
                {
                    CityStorage.cities[i].population = Random.Range(0, 16);
                }
            }

            timePassed = 0;
        }*/
    }
}
