﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors;

public class ResourceManagement : MonoBehaviour
{
    /*
	float timeSinceAllocation;
    static int industryCap;
    static int militaryCap;
    static int propagandaCap;
    static int welfareCap;

    //These next few variables account for changes in the cap due to strategies that have been employed
    public static int industryCapChange;
    public static int militaryCapChange;
    public static int propagandaCapChange;
    public static int welfareCapChange;

    //This variable is needed because I cannot call the function that allocates resources to governors in the start function since
    //the currentGovernors array hasn't been filled with governors yet
    bool start = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceAllocation += Time.deltaTime;
        //This if statement's body executes the function that allocates resources to the governors every 20 seconds
        if (timeSinceAllocation > 10 || start == true)
        {
            industryCap = 40 + industryCapChange;
            militaryCap = 40 + militaryCapChange;
            propagandaCap = 40 + propagandaCapChange;
            welfareCap = 40 + welfareCapChange;
            Debug.Log("Industry cap: " + industryCap + "\nPropaganda cap: " + propagandaCap + "\nWelfare cap: " + welfareCap + "\nMilitary cap: " + militaryCap);
            AssignResources(industryCap, militaryCap, propagandaCap, welfareCap);

            timeSinceAllocation = 0;
            start = false;
        }
    }

    //This function allocates resources to the governors from a random number between 1 and 10 for each resource.
    public void AssignResources(int ind, int mil, int prop, int welf)
    {
        for(int i = 0; i < GovernorStorage.NumOfGovernors(); i++)
        {
            int randomIndustry = Random.Range(1, 10);
            int randomMilitary = Random.Range(1, 10);
            int randomPropaganda = Random.Range(1, 10);
            int randomWelfare = Random.Range(1, 10);

            if (industryCap - randomIndustry > 0) { industryCap -= randomIndustry; }
            if (militaryCap - randomMilitary > 0) { militaryCap -= randomMilitary; }
            if (propagandaCap - randomPropaganda > 0) { propagandaCap -= randomPropaganda; }
            if (welfareCap - randomWelfare > 0) { welfareCap -= randomWelfare; }

            GovernorStorage.currentGovernors[i].resourceCap = new ResourceTraitPack();
			GovernorStorage.currentGovernors[i].resourceCap.industry = randomIndustry;
			GovernorStorage.currentGovernors[i].resourceCap.propaganda = randomPropaganda;
			GovernorStorage.currentGovernors[i].resourceCap.welfare = randomWelfare;
			GovernorStorage.currentGovernors[i].resourceCap.military = randomMilitary;

            //Debug.Log(GovernorStorage.currentGovernors[i].resourceCap + "\n" + GovernorStorage.currentGovernors[i].name);
        }
        //Debug.Log("Industry cap: " + industryCap + "\nPropoganda cap: " + propagandaCap + "\nWelfare cap: " + welfareCap + "\nMilitary cap: " + militaryCap);
    }
	*/
}
