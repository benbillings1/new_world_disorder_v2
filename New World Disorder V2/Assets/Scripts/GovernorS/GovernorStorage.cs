﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors;
using Territories;

public class GovernorStorage : MonoBehaviour
{
	static string strategiesTSVFilePath = "Assets/TSVs/Strategies.tsv";

	static Strategy[] strategies;

	float timeUntilNextDecision = 0f;
	float timeBetweenDecisions = 10f;

	public static Governor[] currentGovernors;
	//public static bool governorsLoaded = false;
	public static List<string> governorNames = new List<string>() { "Ben", "Sarah", "Chris", "Geun", "Hank", "Kameron", "Mike", "Ted", "Will", "Rush", "Norbert", "Chabane" };
	public static string[] regionStorage = { "North America", "South America", "Africa", "Antarctica", "Europe", "Asia", "Australia" };
    public static string[] abrRegionStorage = { "NA", "SA", "AF", "AN", "EU", "AS", "AU" };
    public static Strategy[] strategyStorage = {};

	public static bool strategiesLoaded = false;

	float setFloat(string datum){
		float number;
		if (float.TryParse(datum, out number)){
			return number;
		}
		return 1337f;
	}

	// Start is called before the first frame update
	void Start()
	{
		//Instantiates strategies
		strategiesLoaded = true;
		strategyStorage = new Strategy[100];

        //This for loop gets information from the city tsv file and adds city objects to an array for every city in the game
		for (int row = 3; row < strategyStorage.Length - 1; row++){
			if (TSVScript.read(strategiesTSVFilePath, row, 1) != null){
				Strategy strat = new Strategy();
				strat.name = TSVScript.read(strategiesTSVFilePath, row, 1).ToString();
				//Debug.Log("Naming: " + strat.name);
				strat.description = TSVScript.read(strategiesTSVFilePath, row, 2).ToString();
				
				strat.qualMin = new CityMetricTraitPack();
				strat.qualMin.economy = setFloat(TSVScript.read(strategiesTSVFilePath, row, 3));
				strat.qualMin.life = setFloat(TSVScript.read(strategiesTSVFilePath, row, 5));
				strat.qualMin.education = setFloat(TSVScript.read(strategiesTSVFilePath, row, 7));
				strat.qualMin.approval = setFloat(TSVScript.read(strategiesTSVFilePath, row, 9));
				strat.qualMin.crime = setFloat(TSVScript.read(strategiesTSVFilePath, row, 11));
				//setMetric(strat.qualMin);
				
				strat.qualMax = new CityMetricTraitPack();
				strat.qualMax.economy = setFloat(TSVScript.read(strategiesTSVFilePath, row, 4));
				strat.qualMax.life = setFloat(TSVScript.read(strategiesTSVFilePath, row, 6));
				strat.qualMax.education = setFloat(TSVScript.read(strategiesTSVFilePath, row, 8));
				strat.qualMax.approval = setFloat(TSVScript.read(strategiesTSVFilePath, row, 10));
				strat.qualMax.crime = setFloat(TSVScript.read(strategiesTSVFilePath, row, 12));
				//setMetric(strat.qualMax);
			
				strat.recipeMetrics = new CityMetricTraitPack();
				strat.recipeMetrics.economy =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 13));
				strat.recipeMetrics.life =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 14));
				strat.recipeMetrics.education =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 15));
				strat.recipeMetrics.approval =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 16));
				strat.recipeMetrics.crime =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 17));
				//setMetric(strat.recipeMetrics);
			
				strat.recipeResources = new ResourceTraitPack();
				strat.recipeResources.industry =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 18));
				strat.recipeResources.propaganda =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 19));
				strat.recipeResources.welfare =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 20));
				strat.recipeResources.military =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 21));
				//Debug.Log("Industry: " + strat.recipeResources.industry);
				//setResource(strat.recipeResources);
				
				strat.endMetrics = new CityMetricTraitPack();
				strat.endMetrics.economy =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 22));
				strat.endMetrics.life =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 23));
				strat.endMetrics.education =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 24));
				strat.endMetrics.approval =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 25));
				strat.endMetrics.crime =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 26));
				//setMetric(strat.endMetrics);

				strat.endResources = new ResourceTraitPack();
				strat.endResources.industry =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 27));
				strat.endResources.propaganda =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 28));
				strat.endResources.welfare =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 29));
				strat.endResources.military =  setFloat(TSVScript.read(strategiesTSVFilePath, row, 30));
				//setMetric(strat.endResources);
				
				strategyStorage[row-3] = strat;
			}
		}
			
		string getNew(string name, Strategy[] S){
			for (int i = 0; i < S.Length-1; i++){
				if (S[i] != null && name != null && S[i].name != null && name == S[i].name){
					Strategy newStrat = S[Random.Range(0, S.Length)];
					if (newStrat != null){
						string newName = newStrat.name;
						return getNew(newName, S);
					}
				}
			}
			return name;
		}
		
		//Creates governors at beginning of game
		currentGovernors = new Governor[regionStorage.Length];
		for (int row = 0; row <= regionStorage.Length - 1; row++)
		{
			Governor gov = new Governor();
            gov.name = governorNames[Random.Range(0, governorNames.Count)];
			gov.region = regionStorage[row];
            gov.regionAbreviation = abrRegionStorage[row];
            governorNames.Remove(gov.name);
            
            //Debug.Log(gov);
			
			int stratCount = Random.Range(1, strategyStorage.Length);
			//Debug.Log("Strat Max: " + strategyStorage.Length);
			gov.strategies = new Strategy[stratCount-1];
			
			ResourceTraitPack resourceCap = new ResourceTraitPack();
			resourceCap.industry = 100;
			resourceCap.propaganda = 100;
			resourceCap.welfare = 100;
			resourceCap.military = 100;
			
			gov.resourceCap = resourceCap;

			
			//Debug.Log("Gov:");
			for (int i = 0; i <= stratCount; i++){
				Strategy currentStrat = strategyStorage[Random.Range(0, gov.strategies.Length)];
                //Debug.Log(currentStrat.name);
                if (currentStrat != null)
                {
                    string firstName = currentStrat.name;
                    //Debug.Log("First Name: " + firstName);
                    string newName = getNew(firstName, gov.strategies);
                    for (int j = 0; j <= strategyStorage.Length - 1; j++)
                    {
                        if (newName != "" && strategyStorage[j] != null && newName != null && strategyStorage[j].name == newName && j < strategyStorage.Length && gov.strategies.Length > j)
                        {
                            gov.strategies[j] = strategyStorage[j];
                        }
                    }
                }
            }

            currentGovernors[row] = gov;
        }
	}

	int getArrayLocation(string [] array, string text){
		for (int i = 0; i < array.Length; i++){
			if (array[i] == text){
				return i;
			}
		}
		return -100; //pretty much null
	}

	float resourceScore(Strategy strat, ResourceTraitPack resourceCap, Strategy [] resourceLedger){
		float score = 0;
		
		float indPercent = strat.recipeResources.industry/resourceCap.industry;
		float propPercent = strat.recipeResources.propaganda/resourceCap.propaganda;
		float welfPercent = strat.recipeResources.welfare/resourceCap.welfare;
		float milPercent = strat.recipeResources.military/resourceCap.military;
		
		float availableIndPercent = 0;
		float availablePropPercent = 0;
		float availableWelfPercent = 0;
		float availableMilPercent = 0;
		
		for (int i=0; i < resourceLedger.Length; i++){
			Strategy s = resourceLedger[i];
			availableIndPercent += s.recipeResources.industry;
			availablePropPercent += s.recipeResources.propaganda;
			availableWelfPercent += s.recipeResources.welfare;
			availableMilPercent += s.recipeResources.military;
		}
		
		availableIndPercent = availableIndPercent/resourceCap.industry;
		availablePropPercent = availablePropPercent/resourceCap.propaganda;
		availableWelfPercent = availableWelfPercent/resourceCap.welfare;
		availableMilPercent = availableMilPercent/resourceCap.military;
		
		float indCost = indPercent/availableIndPercent;
		float propCost = propPercent/availablePropPercent;
		float welfCost = welfPercent/availablePropPercent;
		float milCost = milPercent/availableMilPercent;
		
		
		if (indCost > 1 || propCost > 1 || welfCost > 1 || milCost > 1){ //if the cost is above 100%, don't choose it
			return -10000000; //so that this option is never chosen, the score is tanked
		}
		
		score = ((1 - indCost) + (1 - propCost) + (1 - welfCost) + (1 - milCost)) * 50f;
		return score;
	}


	
	float agendaScore(City city, Agenda playerAgenda){

		float sumAgenda(Agenda agenda, CityMetricTraitPack metric){
			
			float economyScore = Mathf.Abs(metric.economy - getArrayLocation(agenda.metricRanking, "Economy"));
			float lifeScore = Mathf.Abs(metric.life - getArrayLocation(agenda.metricRanking, "Life"));
			float educationScore = Mathf.Abs(metric.education - getArrayLocation(agenda.metricRanking, "Education"));
			float approvalScore = Mathf.Abs(metric.approval - getArrayLocation(agenda.metricRanking, "Approval"));
			float crimeScore = -1f * Mathf.Abs(metric.crime - getArrayLocation(agenda.metricRanking, "Crime")); //negative due to being a bad thing when high
			
			if (economyScore < 0 || lifeScore < 0 || educationScore < 0 || approvalScore < 0 || crimeScore > 0){
				return 0;
			}
			
			return 20f-(economyScore + lifeScore + educationScore + approvalScore + crimeScore);
		}
					
		CityMetricTraitPack cityMetric = city.metrics;
		Agenda cityAgenda = city.agenda;
		
		float cityAgendaScore = sumAgenda(cityAgenda, cityMetric);
		float playerAgendaScore = sumAgenda(playerAgenda, cityMetric); //the score based on player agendas
		
		float agendaScore = playerAgendaScore + cityAgendaScore;
		
		return agendaScore;
	}

    float metricScore(Strategy strat, City city){

		float econMin = strat.qualMin.economy;
		float lifeMin = strat.qualMin.life;
		float educMin = strat.qualMin.education;
		float apprMin = strat.qualMin.approval;
		float crimMin = strat.qualMin.crime;

		float econMax = strat.qualMax.economy;
		float lifeMax = strat.qualMax.life;
		float educMax = strat.qualMax.education;
		float apprMax = strat.qualMax.approval;
		float crimMax = strat.qualMax.crime;
	
		float getScore(float focusedMetric,  float min, float max){
			float score = Mathf.Abs(focusedMetric - (min + ((max - min) / 2))); //score based on how qualified the strategy is based on current city metric positioning;
			return score;
		}
			
		float econScore = getScore(city.metrics.economy, econMin, econMax);
		float lifeScore = getScore(city.metrics.life, lifeMin, lifeMax);
		float educScore = getScore(city.metrics.education, educMin, educMax);
		float apprScore = getScore(city.metrics.approval, apprMin, apprMax);
		float crimScore = getScore(city.metrics.crime, crimMin, crimMax);	
	
		return econScore + lifeScore + educScore + apprScore + crimScore;
	}

	//does a chosen cmtp metric land between two areas.
	bool metricQualifier(CityMetricTraitPack qualMin, CityMetricTraitPack qualMax, CityMetricTraitPack cityCurrent){
		bool safe = true;
		if (qualMin.economy != 1337f){
			if (cityCurrent.economy < qualMin.economy || cityCurrent.economy > qualMax.economy){
				safe = false;
			}
		}
		if (qualMin.life != 1337f){
			if (cityCurrent.life < qualMin.life || cityCurrent.life > qualMax.life){
				safe = false;
			}
		}
		if (qualMin.education != 1337f){
			if (cityCurrent.education < qualMin.education || cityCurrent.education > qualMax.education){
				safe = false;
			}
		}
		if (qualMin.approval != 1337f){
			if (cityCurrent.approval < qualMin.approval || cityCurrent.approval > qualMax.approval){
				safe = false;
			}
		}
		if (qualMin.crime != 1337f){
			if (cityCurrent.crime < qualMin.crime || cityCurrent.crime > qualMax.crime){
				safe = false;
			}	
		}
		return safe;
	}

	Strategy makeDecision(Governor gov, City city){
		float smallestMetricScore = float.MaxValue;
		bool setMetric = false;
		Strategy chosenStrat = null;
		
		//find the best strategy based on metric effect

		for (int j=0; j<= gov.strategies.Length-1; j++){
			Strategy strat = gov.strategies[j];
			if (strat != null){

                float totalScore = metricScore(strat, city) + agendaScore(city, gov.playerAgenda) + resourceScore(strat, gov.resourceCap, gov.resourceLedger);
				
				//find best scoring strategy
				if (totalScore < smallestMetricScore){
					setMetric = true;
					smallestMetricScore = totalScore;
					chosenStrat = strat;
				}
			}
		}
		
		if (setMetric == true && city.name != ""){
			return chosenStrat;		
		}else{
			return null;
		}
	}

	bool canAfford(Governor gov, Strategy strat){
		float industryMax = gov.resourceCap.industry;
		float propagandaMax = gov.resourceCap.propaganda;
		float welfareMax = gov.resourceCap.welfare;
		float militaryMax = gov.resourceCap.military;
		
		float industry = strat.recipeResources.industry;
		float propaganda = strat.recipeResources.propaganda;
		float welfare = strat.recipeResources.welfare;
		float military = strat.recipeResources.military;
		
		if (industry >= 1337f){
			industry = 0f;
		}
		if (propaganda >= 1337f){
			propaganda = 0f;
		}
		if (welfare >= 1337f){
			welfare = 0f;
		}
		if (military >= 1337f){
			military = 0f;
		}
		
		for (int i=0; i<=gov.resourceLedger.Length-1; i++){
			if (gov.resourceLedger[i] != null){
				ResourceTraitPack entry = gov.resourceLedger[i].recipeResources;
				
				if (entry.industry < 1337f){
					industry += entry.industry;
				}
				if (entry.propaganda < 1337f){
					propaganda += entry.propaganda;
				}
				if (entry.welfare < 1337f){
					welfare += entry.welfare;
				}
				if (entry.military < 1337f){
					military += entry.military;
				}	
			}
		}
		/*
		Debug.Log("Check");
		Debug.Log(industryMax + " : " + industry);
		Debug.Log(propagandaMax + " : " + propaganda);
		Debug.Log(welfareMax + " : " + welfare);
		Debug.Log(militaryMax + " : " + military);
		*/
		if(industryMax < industry || propagandaMax < propaganda || welfareMax < welfare || militaryMax < military){
			//Debug.Log("False");
			return false;
		}
		//Debug.Log("True");
		return true;
	}

	void updateGovernorCap(ResourceTraitPack rtp, Governor g){
		//make a new resource trait pack
		ResourceTraitPack newRtp = new ResourceTraitPack();
		
		//adjusting the variables to be a combination between the old values and the changes
		newRtp.industry = g.resourceCap.industry + newRtp.industry;
		newRtp.propaganda = g.resourceCap.propaganda + newRtp.propaganda;
		newRtp.welfare = g.resourceCap.welfare + newRtp.welfare;
		newRtp.military = g.resourceCap.military + newRtp.military;
		
		//replace old variable with new trait pack
		g.resourceCap = newRtp;
	}

	// Update is called once per frame
	void Update()
	{
		timeUntilNextDecision -= Time.deltaTime;
		if (timeUntilNextDecision <= 0){
			timeUntilNextDecision = timeBetweenDecisions;
			for (int j=0; j<=currentGovernors.Length-1; j++){
				Governor gov = currentGovernors[j];
				if (gov != null){
					for (int i=0; i<=CityStorage.cities.Length-1; i++){
						City city = CityStorage.cities[i];
						if (city != null){
							if (city.region == gov.region){
								Strategy strat = makeDecision(gov, city);
								if (strat != null){
									if (canAfford(gov, strat) == true){
										Debug.Log(gov.name + " decided to imploy the " + strat.name + " strategy in " + city.name);
										bool found = false;
										for (int k=0; k<=gov.resourceLedger.Length-1; k++){
											Strategy s = gov.resourceLedger[k];
										
											if (s == null && found == false){
												found = true; 
												
												//cloning strategy variable
												Strategy newStrat = new Strategy();
												newStrat.name = strat.name;
												newStrat.description = strat.description;
												newStrat.startTick = Time.realtimeSinceStartup;
												newStrat.duration = strat.duration;
												newStrat.qualMin = strat.qualMin;
												newStrat.qualMax = strat.qualMax;
												newStrat.recipeMetrics = strat.recipeMetrics;
												newStrat.recipeResources = strat.recipeResources;
												newStrat.endMetrics = strat.endMetrics;
												newStrat.endResources = strat.endResources;
											
												city.metrics.economy -= newStrat.recipeMetrics.economy;
												city.metrics.life -= newStrat.recipeMetrics.life;
												city.metrics.education -= newStrat.recipeMetrics.education;
												city.metrics.approval -= newStrat.recipeMetrics.approval;
												city.metrics.crime += newStrat.recipeMetrics.crime;
													
												newStrat.city = city;
												
												gov.resourceLedger[k] = newStrat;
												break;
											}
										
										}
									}
                                }
								
							}
						}
					}
				}
			}
		}

		//removes strat 
		for (int j=0; j<=currentGovernors.Length-1; j++){
			Governor gov = currentGovernors[j];
			if (gov != null){
				for (int i=0; i<=gov.resourceLedger.Length-1; i++){
					Strategy strat = gov.resourceLedger[i];
					if (strat != null){
						float timeSince = Time.realtimeSinceStartup - strat.startTick;
						if (timeSince > strat.duration){
							strat.city.metrics.economy += strat.endMetrics.economy;
							strat.city.metrics.life += strat.endMetrics.life;
							strat.city.metrics.education += strat.endMetrics.education;
							strat.city.metrics.approval += strat.endMetrics.approval;
							strat.city.metrics.crime -= strat.endMetrics.crime;

							gov.resourceCap.industry += strat.endResources.industry;
							gov.resourceCap.propaganda += strat.endResources.propaganda;
							gov.resourceCap.welfare += strat.endResources.welfare;
							gov.resourceCap.military += strat.endResources.military;
							
							gov.resourceLedger[i] = null;
						}
					}
				}
			}
		}

	}
}

