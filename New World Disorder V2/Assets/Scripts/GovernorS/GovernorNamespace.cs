﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Territories;

namespace Governors
{
    public class CityMetricTraitPack
    {
        public float economy;
        public float life;
        public float education;
        public float approval;
        public float crime;

        public CityMetricTraitPack(){}

    }

    public class ResourceTraitPack
    {
        public float industry = 1;
        public float propaganda = 1;
        public float welfare = 1;
        public float military = 1;

        public ResourceTraitPack(){}

        public override string ToString()
        {
            return "Industry: " + industry + "\nPropaganda: " + propaganda + "\nWelfare: " + welfare + "\nMilitary: " + military;
        }
    }

	public class Agenda
	{
		public string[] metricRanking = new string[5];
		public string[] resourceRanking = new string[4];
		
		public Agenda(){
			metricRanking = new string[]{"Economy", "Life", "Education", "Approval", "Crime"};
			resourceRanking = new string[]{"industry", "propaganda", "welfare", "military"};
			
		}
		public string[] rank(string traitName, int r, bool isMetric){
			string [] oldArray;
			string [] newArray;
			if (isMetric == true){
				oldArray = metricRanking;
				newArray = new string[5];
			}else{
				oldArray = resourceRanking;
				newArray = new string[4];
			}
			for (int i = 0; i < newArray.Length; i++){
				if (i < r){
					newArray[i] = oldArray[i];
				}else if(i == r){
					newArray[i] = oldArray[r];
				}else if(i > r){
					newArray[i] = oldArray[i-1];
				}
			}
			return newArray;
		}
	}

    public class Strategy
    {
        public string name;
        public string description;
		public float startTick;
		public float duration;
		
		//qualification for strategy
		public CityMetricTraitPack qualMin;
		public CityMetricTraitPack qualMax;
		
		//how much it takes to complete the strategy
		public CityMetricTraitPack recipeMetrics;
		public ResourceTraitPack recipeResources;
		
		//given once the strategy completes
		public CityMetricTraitPack endMetrics;
		public ResourceTraitPack endResources;		
		
		public City city; //where the strategy is being applied

        public Strategy(){}

    }

    public class Governor
    {
        public string name;
        public string region;
        public string regionAbreviation;
        public Strategy[] strategies;
        public ResourceTraitPack resourceCap;
		public Agenda playerAgenda;
		public Strategy [] resourceLedger = new Strategy[1000];
        public Governor(){
			
			resourceCap = new ResourceTraitPack();
			playerAgenda = new Agenda();
		}

        public override string ToString()
        {
            return "Name: " + name + " Region: " + region;
        }
    }
}
