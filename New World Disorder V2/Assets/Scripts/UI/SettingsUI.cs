﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    public GameObject hideUIButton;
    public GameObject showSettingsButton;
    public GameObject hideSettingsButton;
    public GameObject settingsUI;
    public GameObject mainUI;
    public GameObject resourceUI;
    public GameObject canvas;

    float originalX;
    bool isMenuVisible = true;
    


    public void HideMainUI()
    {
        gameObject.GetComponent<RectTransform>().position = new Vector3(gameObject.GetComponent<RectTransform>().position.x + (canvas.GetComponent<RectTransform>().rect.width * .123f), gameObject.transform.position.y, 0);
    }

    public void ShowMainUI()
    {
        gameObject.transform.position = new Vector3(originalX, gameObject.transform.position.y, 0);
    }

    public void ChangeUIVisibility()
    {

        if (isMenuVisible)
        {
            HideMainUI();
            isMenuVisible = false;
        }
        else
        {
            ShowMainUI();
            isMenuVisible = true;
        }
    }

    public void ShowSettingsUI()
    {
        settingsUI.SetActive(true);
        mainUI.SetActive(false);
        resourceUI.SetActive(false);

        GlobeScript.earthLocked = true;

    }

    public void HideSettingsUI()
    {
        settingsUI.SetActive(false);
        mainUI.SetActive(true);
        resourceUI.SetActive(true);

        GlobeScript.earthLocked = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        originalX = gameObject.transform.position.x;
        hideUIButton.GetComponent<Button>().onClick.AddListener(ChangeUIVisibility);
        showSettingsButton.GetComponent<Button>().onClick.AddListener(ShowSettingsUI);
        hideSettingsButton.GetComponent<Button>().onClick.AddListener(HideSettingsUI);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
