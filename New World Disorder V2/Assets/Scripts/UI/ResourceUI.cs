﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Governors;
using Territories;

public class ResourceUI : MonoBehaviour
{
	public ResourceTraitPack resources;
	public Strategy[] resourceLedger;

	public GameObject industryBar;
	public GameObject propagandaBar;
	public GameObject welfareBar;
	public GameObject militaryBar;

	void updateBar(GameObject ui, float percentage){
		float maxBar = 129f;
		float size = (1f-percentage)*maxBar;
		//Debug.Log(size);
		if (ui != null){
			ui.GetComponent<RectTransform>().offsetMax = new Vector2(12f, -size);
		}
	}  

	public void updateUI()
	{

	
		float industryLimit = resources.industry;
		float propagandaLimit = resources.propaganda;
		float welfareLimit = resources.welfare;
		float militaryLimit = resources.military;
		
		float industry = 0;
		float propaganda = 0;
		float welfare = 0;
		float military = 0;
		
		if (industry >= 1337f){
			industry = 0f;
		}
		if (propaganda >= 1337f){
			propaganda = 0f;
		}
		if (welfare >= 1337f){
			welfare = 0f;
		}
		if (military >= 1337f){
			military = 0f;
		}
		
		for (int i=0; i<=resourceLedger.Length-1; i++){
			if (resourceLedger[i] != null){
				ResourceTraitPack entry = resourceLedger[i].recipeResources;
				
				if (entry.industry < 1337f){
					industry += entry.industry;
				}
				if (entry.propaganda < 1337f){
					propaganda += entry.propaganda;
				}
				if (entry.welfare < 1337f){
					welfare += entry.welfare;
				}
				if (entry.military < 1337f){
					military += entry.military;
				}	
			}
		}
		
		updateBar(industryBar, industry/industryLimit);
		updateBar(propagandaBar, propaganda/propagandaLimit);
		updateBar(welfareBar, welfare/welfareLimit);
		updateBar(militaryBar, military/militaryLimit);
	}
	
	void Update()
	{
		updateUI();
	}
	
	void Start()
	{
		resources = new ResourceTraitPack();
		resourceLedger = new Strategy[1000];
	}	
}
