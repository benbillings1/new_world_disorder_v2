﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Governors;
using UnityEngine.EventSystems;

public class AgendaUI : MonoBehaviour
{
    public GameObject govAgendas;
    public GameObject govStatus;
    public GameObject govManagement;
    public GameObject popularityLeaderboard;

    public GameObject backButton;

    public GameObject ScrollView;
    public GameObject ScrollViewContent;
    public GameObject governorInfoButton;
    public GameObject governorChoiceButton;
    public GameObject agendaButton;
    public GameObject informationHeader;
    public GameObject agendaCutOut;
    public GameObject buttonStorage;
    public GameObject newObjectStorage;

    public GameObject panelScrollView;
    public GameObject panelScrollViewContent;
    public GameObject governorNews;
    public GameObject govInfoPanel;
    public GameObject posAttribute1;
    public GameObject negAttribute1;

    GameObject getChild(GameObject Obj, string name)
    {
        //Debug.Log(Obj.name);
        foreach (Transform o in Obj.transform)
        {
            //Debug.Log(o.gameObject.name + " : " + name);
            if (o.gameObject.name == name)
            {
                return o.gameObject;
            }
        }
        return null;
    }

    //Clears Scrollview content
    void DeleteContents(GameObject obj)
    {
        foreach(Transform tran in obj.transform)
        {
            Destroy(tran.gameObject);
        }
    }

    void SetLeaderboardUIActive()
    {
        govAgendas.SetActive(false);
        govStatus.SetActive(false);
        govManagement.SetActive(false);
        popularityLeaderboard.SetActive(false);

        backButton.SetActive(true);
        ScrollView.SetActive(true);
        informationHeader.SetActive(true);

        GlobeScript.earthLocked = true;
        
        int count = 1;
        informationHeader.GetComponent<Text>().text = "Governor Popularity Leaderboard";

        DeleteContents(ScrollViewContent);

        foreach(Governor gov in GovernorStorage.currentGovernors)
        {
            GameObject currentGov = Instantiate(governorInfoButton, ScrollViewContent.transform);

            getChild(currentGov, "Rank").GetComponent<Text>().text = count + "";
            if (count == 1)
            {
                getChild(currentGov, "Rank").GetComponent<Text>().color = Color.yellow;
            }
            getChild(currentGov, "Name").GetComponent<Text>().text = gov.name + ": " + gov.region;

            currentGov.GetComponent<Button>().onClick.AddListener(delegate { SetGovernorInfoActive(gov.name, gov.regionAbreviation); });
            currentGov.SetActive(true);

            count++;
        }
    }

    //This method sets the information in the panel that appears when a directive button has been clicked on. 
    //Right now it just assigns a textbox the name of the directive clicked on.
    void SetGovernorInfoActive(string govName, string govRegion)
    {
        getChild(govInfoPanel, "Name").GetComponent<Text>().text = govName + ": " + govRegion;
        getChild(govInfoPanel, "Details").GetComponent<Text>().text = "Governor Info";
        getChild(govInfoPanel, "Details").GetComponent<Text>().color = Color.white;
        getChild(govInfoPanel, "Details").GetComponent<Text>().fontSize = 14;

        govInfoPanel.SetActive(true);
        panelScrollView.SetActive(false);
        posAttribute1.SetActive(false);
        negAttribute1.SetActive(false);
    }

    void SetGovProfileActive(string govName, string govRegion)
    {
        getChild(govInfoPanel, "Name").GetComponent<Text>().text = govName + ": " + govRegion;
        getChild(govInfoPanel, "Details").GetComponent<Text>().text = "Recent Events: ";
        getChild(govInfoPanel, "Details").GetComponent<Text>().color = Color.yellow;
        getChild(govInfoPanel, "Details").GetComponent<Text>().fontSize = 20;

        DeleteContents(panelScrollViewContent);

        GameObject govNews = Instantiate(governorNews, panelScrollViewContent.transform);
        getChild(govNews, "Text").GetComponent<Text>().text = "This is where governor news goes";
        getChild(posAttribute1, "Text").GetComponent<Text>().text = "Positive Gov Trait";
        getChild(negAttribute1, "Text").GetComponent<Text>().text = "Negative Gov Trait";

        govInfoPanel.SetActive(true);
        panelScrollView.SetActive(true);
        posAttribute1.SetActive(true);
        negAttribute1.SetActive(true);


    }

    //This function makes the directive priority UI active for the governor chosen on the previous UI, which is the governor directive UI. 
    //The function implemented after this one handles the governor directive UI. 
    void SetAgendaPriorityUIActive(string govName, string govRegion)
    {
        informationHeader.GetComponent<Text>().text = "Setting Agendas For " + govName;

        backButton.GetComponent<Button>().onClick.RemoveAllListeners();
        backButton.GetComponent<Button>().onClick.AddListener(SetGovernorAgendaUIActive);

        ScrollView.SetActive(false);
		
		Governor governor = new Governor();
		
		foreach(Governor gov in GovernorStorage.currentGovernors)
        {
			//Debug.Log(gov.name + " : " + govName);
			if (gov.name == govName){
				governor = gov;
			}
		}

		string[] agendas = governor.playerAgenda.metricRanking;

        float yLocation = 0;

        for (int i = 1; i <= 5; i++)
        {
            yLocation -= gameObject.GetComponent<RectTransform>().rect.height * .1f;
            Debug.Log(gameObject.GetComponent<RectTransform>().rect.height * .1f);
            Debug.Log(gameObject.GetComponent<RectTransform>().rect.height);

            GameObject agendCutOut = Instantiate(agendaCutOut, informationHeader.transform);
            agendCutOut.GetComponent<RectTransform>().localPosition = new Vector3(agendCutOut.GetComponent<RectTransform>().localPosition.x, yLocation, 0);
            agendCutOut.name = "Agenda Cut Out " + i;

            GameObject currentAgend = Instantiate(agendaButton, buttonStorage.transform);
            currentAgend.GetComponent<RectTransform>().localPosition = new Vector3(currentAgend.GetComponent<RectTransform>().localPosition.x, yLocation, 0);
            getChild(currentAgend, "Rank").GetComponent<Text>().text = i + "";
            getChild(currentAgend, "Name").GetComponent<Text>().text = agendas[i - 1];
            getChild(currentAgend, "Value").GetComponent<Text>().text = "0";
            currentAgend.name = agendas[i - 1];

            currentAgend.SetActive(true);
        }
    }

    //This function makes the UI screen where you choose a governor to set directives for active
    void SetGovernorAgendaUIActive()
    {
        govAgendas.SetActive(false);
        govStatus.SetActive(false);
        govManagement.SetActive(false);
        popularityLeaderboard.SetActive(false);

        backButton.SetActive(true);
        ScrollView.SetActive(true);
        informationHeader.SetActive(true);
        buttonStorage.SetActive(true);

        backButton.GetComponent<Button>().onClick.RemoveAllListeners();
        backButton.GetComponent<Button>().onClick.AddListener(SetMainUIActive);

        GlobeScript.earthLocked = true;
        
        informationHeader.GetComponent<Text>().text = "Choose a Governor";

        DeleteContents(informationHeader);
        DeleteContents(buttonStorage);
        DeleteContents(ScrollViewContent);
        DeleteContents(newObjectStorage);

        Debug.Log(GovernorStorage.currentGovernors[0]);

        foreach (Governor gov in GovernorStorage.currentGovernors)
        {
            Debug.Log(gov);
            GameObject currentGov = Instantiate(governorChoiceButton, ScrollViewContent.transform);

            getChild(currentGov, "Name").GetComponent<Text>().text = gov.name + ": " + gov.region;

            currentGov.GetComponent<Button>().onClick.AddListener(delegate { SetAgendaPriorityUIActive(gov.name, gov.region); });
        } 
    }

    void SetGovernorStatusUIActive()
    {
        govAgendas.SetActive(false);
        govStatus.SetActive(false);
        govManagement.SetActive(false);
        popularityLeaderboard.SetActive(false);

        backButton.SetActive(true);
        informationHeader.SetActive(true);
        ScrollView.SetActive(true);

        backButton.GetComponent<Button>().onClick.RemoveAllListeners();
        backButton.GetComponent<Button>().onClick.AddListener(SetMainUIActive);

        GlobeScript.earthLocked = true;

        informationHeader.GetComponent<Text>().text = "Choose a Governor";

        DeleteContents(informationHeader);
        DeleteContents(panelScrollViewContent);
        DeleteContents(ScrollViewContent);


        Debug.Log(GovernorStorage.currentGovernors[0]);

        foreach (Governor gov in GovernorStorage.currentGovernors)
        {
            Debug.Log(gov);
            GameObject currentGov = Instantiate(governorChoiceButton, ScrollViewContent.transform);

            getChild(currentGov, "Name").GetComponent<Text>().text = gov.name + ": " + gov.region;

            currentGov.GetComponent<Button>().onClick.AddListener(delegate { SetGovProfileActive(gov.name, gov.region); });
        }
    }

    void SetMainUIActive()
    {
        govAgendas.SetActive(true);
        govStatus.SetActive(true);
        govManagement.SetActive(true);
        popularityLeaderboard.SetActive(true);

        backButton.SetActive(false);
        ScrollView.SetActive(false);
        informationHeader.SetActive(false);
        govInfoPanel.SetActive(false);

        GlobeScript.earthLocked = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        govAgendas.GetComponent<Button>().onClick.AddListener(SetGovernorAgendaUIActive);
        govStatus.GetComponent<Button>().onClick.AddListener(SetGovernorStatusUIActive);
        popularityLeaderboard.GetComponent<Button>().onClick.AddListener(SetLeaderboardUIActive);
        backButton.GetComponent<Button>().onClick.AddListener(SetMainUIActive);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
