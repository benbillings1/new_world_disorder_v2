﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using Governors;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    Transform originalLocation;

    GameObject buttonStorage;
    GameObject newObjectStorage;

    GameObject getChild(GameObject Obj, string name)
    {
        //Debug.Log(Obj.name);
        foreach (Transform o in Obj.transform)
        {
            //Debug.Log(o.gameObject.name + " : " + name);
            if (o.gameObject.name == name)
            {
                return o.gameObject;
            }
        }
        return null;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        originalLocation.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0);
        transform.SetSiblingIndex(9);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

	//changing rank area
    public void OnEndDrag(PointerEventData eventData)
    {
        bool withinRange = false;
		
		string govName = getChild(buttonStorage.transform.parent.gameObject, "InformationHeader").GetComponent<Text>().text;
		string preCursor = "Setting Agendas For ";
		govName = govName.Substring((preCursor.Length));
		Debug.Log("Gov: " + govName);	
		
        foreach (Transform o in buttonStorage.transform)
        {
            if (gameObject.name != o.gameObject.name)
            {
                if (Mathf.Abs(transform.position.y - o.position.y) < 20)
                {
                    withinRange = true;

                    string originalRank = getChild(gameObject, "Rank").GetComponent<Text>().text;
					int oldRank = Convert.ToInt32(originalRank) - 1;
					
                    getChild(gameObject, "Rank").GetComponent<Text>().text = getChild(o.gameObject, "Rank").GetComponent<Text>().text;
					int newRank = Convert.ToInt32(getChild(gameObject, "Rank").GetComponent<Text>().text) - 1;

					foreach(Governor governor in GovernorStorage.currentGovernors)
					{
						if (governor.name == govName){
							string oldString = governor.playerAgenda.metricRanking[oldRank];
							string newString = governor.playerAgenda.metricRanking[newRank];	

							governor.playerAgenda.metricRanking[oldRank] = newString;
							governor.playerAgenda.metricRanking[newRank] = oldString;
						}
					}
					
                    getChild(o.gameObject, "Rank").GetComponent<Text>().text = originalRank;
					
                    transform.position = new Vector3(o.position.x, o.position.y, 0);
                    o.position = new Vector3(originalLocation.position.x, originalLocation.position.y, 0);
                }
            }
        }



        if (!withinRange)
        {
            transform.position = new Vector3(originalLocation.position.x, originalLocation.position.y, 0);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        originalLocation = new GameObject().transform;
        originalLocation.position = new Vector3(0, 0, 0);

        buttonStorage = GameObject.Find("AgendaButtonStorage");

        newObjectStorage = GameObject.Find("NewObjectStorage");
        originalLocation.SetParent(newObjectStorage.transform);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
